## Software Requiremnts

- Docker

## How to Run

### To run dev
1. cd app
2. npm i
3. copy env.sample to development.env
4. Update the config value inside development.env
5. Open CMD and run

```
$ docker-compose up --build --remove-orphans -d
```
### Now, app run on port `9000`


## Run DOCKER staging
```
docker-compose -f docker-compose-stage.yml up --build --remove-orphans -d
```