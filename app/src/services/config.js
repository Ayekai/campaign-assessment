import nconf from 'nconf';
import path from 'path';
import { camelCase } from 'lodash';

const envSeparator = '__';
const matchRegex = /^TC_/;

nconf.env({
  separator: envSeparator,
  parseValues: true,
  transform: function(obj) {
    if (!obj.key.match(matchRegex)) {
      return obj;
    }
    obj.key = obj.key.replace(matchRegex, '').toLowerCase();
    const keyParts = obj.key.split(envSeparator);

    const foundElementIndex = keyParts.findIndex((element) => element.includes('_'));
    if (foundElementIndex > -1) {
      keyParts[foundElementIndex] = camelCase(keyParts[foundElementIndex]);
      obj.key = keyParts.join(envSeparator);
    }    
    return obj;
  },
}).argv();

const envConf = process.env.NODE_ENV;

if (envConf && envConf !== 'development') {
  nconf.file('override', path.resolve('.'))
  nconf.file('override', path.resolve('./conf/', `${process.env.NODE_ENV}.json`));
  
}
// Base configuration for all environments
nconf.file('default', path.resolve('./conf/', 'development.json'));

export default nconf;
