import Sequelize from 'sequelize';
import conf from './config';

const dbConfig = conf.get('db');
const enableDbLog = dbConfig.enableDbLog ? console.log : false;

export const dataTypes = {
  TIMESTAMP: 'TIMESTAMP',
  INTEGER: Sequelize.INTEGER,
  STRING: Sequelize.STRING,
  DATE: Sequelize.DATE,
  BOOLEAN: Sequelize.BOOLEAN,
  DECIMAL: Sequelize.DECIMAL,
  ENUM: Sequelize.ENUM
};

export const defaultValues = {    
  CURRENT_TIMESTAMP: Sequelize.literal('CURRENT_TIMESTAMP'),  
};

const dbInstance = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  port: dbConfig.port,
  dialect: dbConfig.dialect,
  logging: enableDbLog,
  define: {
    timestamps: false
  },
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

export default dbInstance;