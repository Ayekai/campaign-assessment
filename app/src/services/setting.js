import Setting from '../models/setting';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

async function findUpdateIssuedCount() {    
  const setting = Setting.increment('issued_count', { where: { [Op.and]: [ Sequelize.literal('issued_count < `limit`') ]}});
  return setting;
}

exports.findUpdateIssuedCount = findUpdateIssuedCount;