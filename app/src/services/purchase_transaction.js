import PurchaseTransaction from '../models/purchase_transaction';
import moment from 'moment-timezone';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

async function findTransactionsByCustomerId(customer_id) {  
  return PurchaseTransaction.findAll({ where: { 
    transaction_at: { [Op.gt]: moment().subtract(30, 'days').toDate() },
    customer_id: customer_id,    
    },
    limit: 3,
    order: [['total_spent', 'DESC']]
  });
}

exports.findTransactionsByCustomerId = findTransactionsByCustomerId;