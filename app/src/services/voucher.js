import Voucher from '../models/voucher';
import Sequelize from 'sequelize';
import moment from 'moment-timezone';
const Op = Sequelize.Op;

async function findAvailableVoucher() {   
  return Voucher.findOne({ where: {   
      used: false,  
      [Op.or]: [{ time_duration: {[Op.lt]: moment().subtract(10, 'minutes').toDate()} }, { time_duration: null }],
    }    
  });
}

async function findBlockedVoucher(customer_id ) {
  return Voucher.findOne({ where: { customer_id: customer_id, time_duration: { [Op.ne]: null } }, raw: true });
}

async function findUpdateBlockVoucherById(id, customer_id) {  
  let now = new Date(Date.now());  

  const voucher = await Voucher.update({ customer_id: customer_id, time_duration: now }, 
    { where: { id: id }, returning: true });
  
  return voucher[1];  
}

async function findUpdateIssuedVoucher(customer_id) {   
  const foundVoucher = await Voucher.findOne({ where: { customer_id: customer_id, time_duration: { [Op.ne]: null } }});
  if (!foundVoucher) return null;
  
  foundVoucher.time_duration = null;
  foundVoucher.used = true;
  let voucher = await foundVoucher.save();
  
  return voucher;  
}

async function findUpdateUnblockVoucher(voucher_id) {  
  const voucher = await Voucher.update({ customer_id: null, time_duration: null }, 
    { where: { id: voucher_id }, returning: true });
  
  return voucher[1];  
}

exports.findAvailableVoucher = findAvailableVoucher;
exports.findUpdateBlockVoucherById = findUpdateBlockVoucherById;
exports.findUpdateIssuedVoucher = findUpdateIssuedVoucher;
exports.findUpdateUnblockVoucher = findUpdateUnblockVoucher;
exports.findBlockedVoucher = findBlockedVoucher;