import Customer from '../models/customer';

async function findCustomerByEmail(email) {
  return Customer.findOne({ where: { email: email }, raw: true });
}

exports.findCustomerByEmail = findCustomerByEmail;