import express from 'express';
import dbConnection from '../../services/db';
import EE from '../../core/error';
import { responseErrorUnknown } from '../../core/error';

const router = express.Router();

let dbStatus = '';

const dateformat = (seconds) => {
  const pad = (s) => {
    return (s < 10 ? '0' : '') + s;
  };
  let hours =  Math.floor(seconds / (60 * 60)),
    minutes = Math.floor(seconds % (60 * 60) / 60),
    lastSeconds = Math.floor(seconds % 60);
  
  return pad(hours) + 'hr:' + pad(minutes) + 'min:' + pad(lastSeconds) + 'sec';
};

const checkDBConnection = () => {  
   return dbConnection.authenticate()
    .then(() => {
      dbStatus = 'DB : ok';      
      
      return dbStatus;
    })
    .catch((err) => {
      dbStatus = 'DB error :' + err;        
      return dbStatus;
    });      

};

router.route('/').get((req, res) => {
  let app = req.app;

  checkDBConnection()
    .then((status) => {
      let healthcheckdata = {
          environmentType: process.env.NODE_ENV,
          "Server UpTime": dateformat(process.uptime()),
          "Environment Status": app.get("env"),
          "DB Connection status": status            
        };

      return res.json({
        status: 200,
        HealthCheckStatus: healthcheckdata          
      });
    })
    .catch((err) => {            
      if (err instanceof EE) {
        return err.sendResponse(res);
      }
  
      logger.error("healthcheck - exception", err);      
      return responseErrorUnknown(res);
    });  
});

module.exports = router;