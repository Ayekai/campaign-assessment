import express from 'express';
import { asyncMiddleware, isValidPeriod } from '../../helpers';
import helper from '../../helpers';
import { check, validationResult } from 'express-validator';
import EE from '../../core/error';
import { responseErrorUnknown } from '../../core/error';
import Setting from '../../models/setting';
import { findCustomerByEmail } from '../../services/customer';
import { findTransactionsByCustomerId } from '../../services/purchase_transaction';
import { findAvailableVoucher, findUpdateBlockVoucherById, findUpdateIssuedVoucher, findUpdateUnblockVoucher, findBlockedVoucher } from '../../services/voucher';
import { findUpdateIssuedCount } from '../../services/setting';
import Promise from 'promise';

const router = express.Router();

router.get('/campaign',     
  asyncMiddleware(async (req, res, next) => {
  
  try {
    const is_valid_campaign = await checkValid();    
    
    if (is_valid_campaign == 200 ) {
      return res.json({
        status: 200,
        message: "success"
      });    
    } else {
      return EE.responseErrorFlow(res, "Sorry! The campaign is currently closed.");      
    }    
  } catch (err) {
    if (err instanceof EE) {
      return err.sendResponse(res);
    }
        
    logger.error("campaign - exception", err);    
    return responseErrorUnknown(res);
  }
}));

router.post('/start_campaign',    
  check('email').isEmail(), 
  asyncMiddleware(async (req, res, next) => {
  
  let errorsResult = validationResult(req);
  
  if (errorsResult['errors'] && errorsResult['errors'].length !== 0) {
    let errMsg = '';
    
    if (errorsResult['errors'][0].param == 'email') {
      errMsg = "Please enter a valid email address.";
    } else {      
      errMsg = "Oops! An unknown error has occurred.";
    }    
    return EE.responseClientError(res, errMsg);
  }

  let email = req.body.email.slice(0).toLowerCase();
  
  try {    
    const customer = await findCustomerByEmail(email);
    
    if (!customer || customer == null) {            
      return EE.responseErrorNotfound(res);
    }
    
    const availabe_voucher = await findAvailableVoucher();
    if (!availabe_voucher) {
      return EE.responseErrorFlow(res, "No longer a valid voucher");
    }

    let transactions = await findTransactionsByCustomerId(customer.id);
      let total = 0;
      
      if (transactions.length <= 2) {        
        return EE.responseErrorFlow(res, "Not eligible");
      }
      transactions.forEach(transaction => {
        total += parseFloat(transaction.total_spent);
      });
  
      if (total < 100) {
        return EE.responseErrorFlow(res, "Not eligible");
      }
            
      await findUpdateBlockVoucherById(availabe_voucher.id, customer.id);  
      
      return res.json({
        status: 200,
        message: "success"
      });

  } catch (err) {
    if (err instanceof EE) {
      return err.sendResponse(res);
    }
        
    logger.error("start campaign - exception", err);    
    return responseErrorUnknown(res);
  }

}));

router.post('/submit_photo',     
  check('email').isEmail(), 
  asyncMiddleware(async (req, res, next) => {
    let errorsResult = validationResult(req);
    if (errorsResult['errors'] && errorsResult['errors'].length !== 0) {
      let errMsg = '';
      
      if (errorsResult['errors'][0].param == 'email') {
        errMsg = "Please enter a valid email address.";
      } else {      
        errMsg = "Oops! An unknown error has occurred.";
      }    
      return EE.responseClientError(res, errMsg);
    }
  
    try {   
      const customer = await findCustomerByEmail(req.body.email);
      
      if (!customer || customer == null) {            
        return EE.responseErrorNotfound(res);
      }

      const valid_image = imageIecognition(); 
      if (valid_image == false) {      
        return EE.responseErrorFlow(res, "Not eligible");
      }

      const found_voucher = await findBlockedVoucher(customer.id);    
      if (!found_voucher) {
        return EE.responseErrorFlow(res, "Not eligible");
      }

      let is_expired = helper.isExipred(found_voucher.time_duration);      

      if (is_expired == true) {        
        await findUpdateUnblockVoucher(found_voucher.id);
        
        return EE.responseErrorFlow(res, "Expired");
      }

      let voucher = await findUpdateIssuedVoucher(customer.id);

      let increment_issued_count = await findUpdateIssuedCount();      

      return res.json({
        status: 200,
        message: voucher.code
      });
      
      
    } catch (err) {
      if (err instanceof EE) {
        return err.sendResponse(res);
      }
          
      logger.error("submit photo - exception", err);    
      return responseErrorUnknown(res);
    }

}));

const checkValid = () => {      
  let isValid = false; 

  return new Promise((resolve, reject) => {
    Setting.findOne()      
      .then((sett) => {             
        isValid = isValidPeriod(sett.start_date, sett.end_date);        
        
        let availabe_voucher = findAvailableVoucher();
        if (isValid == true && sett.issued_count < sett.limit && availabe_voucher) {                    
          resolve(200);
        } else {
          resolve(201);
        }
      })
      .catch((error) => {        
        return reject(error);
      });      
    });
}

const imageIecognition = () => {
  return true;
}


module.exports = router;