import db from '../services/db';
import {dataTypes, defaultValues} from '../services/db';

const Customer = db.define('customers', {
  id: {
    type: dataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  first_name: {
    type: dataTypes.STRING,
    allowNull: true,
  },
  last_name: {
    type: dataTypes.STRING,
    allowNull: true,
  },
  gender: {
    type: dataTypes.STRING,
    allowNull: false
  },
  date_of_birth: {
    type: dataTypes.DATE,
    allowNull: false
  },
  contact_number: {
    type: dataTypes.DATE,
    allowNull: true
  },
  email: {
    type: dataTypes.STRING,
    allowNull: false,
  },
},{
  freezeTableName: true,
},{
  created_at: dataTypes.DATE,
  updated_at: dataTypes.DATE,
});

export default Customer;