import db from '../services/db';
import {dataTypes, defaultValues} from '../services/db';

const purchase_transaction = db.define('purchase_transaction', {
  id: {
    type: dataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },    
  customer_id: {
    type: dataTypes.INTEGER,
    allowNull: false
  },
  total_spent: {
    type: dataTypes.DECIMAL(10,2),
    allowNull: true,      
  },
  total_saving: {
    type: dataTypes.DECIMAL(10,2),
    allowNull: false
  },    
  transaction_at: dataTypes.DATE,    
},{
    freezeTableName: true,
  }
);

export default purchase_transaction;