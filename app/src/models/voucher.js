import db from '../services/db';
import {dataTypes, defaultValues} from '../services/db';

const Voucher = db.define('voucher', {
  id: {
    type: dataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  code: {
    type: dataTypes.STRING,
    allowNull: false
  },
  customer_id: {
    type: dataTypes.INTEGER,
    allowNull: true,      
  },  
  time_duration: {
    type: dataTypes.TIMESTAMP,
    allowNull: true,      
  },  
  used: {
    type: dataTypes.BOOLEAN,
    allowNull: false,      
    defaultValue: false
  },  
});

export default Voucher;