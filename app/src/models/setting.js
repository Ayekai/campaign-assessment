import db from '../services/db';
import {dataTypes, defaultValues} from '../services/db';

const Setting = db.define('setting', {
  id: {
    type: dataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  start_date: dataTypes.DATE,
  end_date: dataTypes.DATE,  
  limit: {
    type: dataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  issued_count: {
    type: dataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
},{
  freezeTableName: true,
});

export default Setting;