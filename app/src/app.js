/* eslint handle-callback-err: "off" */
import express from 'express';
import path from 'path';
import winston from 'winston';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import DB from './services/config';
import Api from './routes/v1';

let app = express();

app.DB = DB;  

global.logger = new (winston.createLogger)({
  transports: [
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: true,
      colorize: true
    })
  ],
  exitOnError: false
});

logger.stream = {
  write: function (message, encoding) {
    logger.info(message);
  }
};

app.use(require("morgan")("combined", { stream: logger.stream }));

app.use(cookieParser());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.use("/api/v1/healthcheck", Api.healthcheck);
app.use("/api/v1/", Api.campaign);

// catch 404 and forward to error handler
app.use((req, res) => {
  res.json({
    status: 404,
    message: "API Not Found"
  });
});

module.exports = app;