class EE extends Error {
  constructor(code, message = null, status = null) {

    // Calling parent constructor of base Error class.
    super(`${code} - ${message}`);

    // Saving class name in the property of our custom error as a shortcut.
    this.name = this.constructor.name;

    this.originalMessage = message;

    // Internal error code
    this.code = code;

    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);

    // You can use any additional properties you want.
    // I'm going to use preferred HTTP status for this error types.
    // `500` is the default value if not specified.
    this.status = status || this.httpStatusHeader();
  }

  httpStatusHeader(code = null) {
    let status = 500;

    switch (this.code) {
      case EE.ERR_FLOW:      
        status = 201;
        break;
      case EE.ERR_NOTFOUND:
      case EE.ERR_FILE_PERMISSION:
        status = 404;
        break;
      case EE.ERR_UNKNOWN:
        status = 500;
        break;
      default:
        status = 500;
        break;
    }

    return status;
  }

  httpStatusResponse(code = null) {
    let status = 500;
    let c = code || this.code;

    if (this.status != null) {
      return this.status;
    }

    switch (c) {
      case EE.ERR_FLOW:
        status = 201;
        break;      
      case EE.ERR_NOTFOUND:
      case EE.ERR_FILE_PERMISSION:
        status = 404;
        break;
      case EE.ERR_UNKNOWN:
        status = 500;
        break;
      default:
        status = 500;
        break;
    }

    return status;
  }

  httpDefaultResponse() {
    let msg = '';

    switch (this.code) {
      case EE.ERR_NOTFOUND:
        msg = "Sorry, this record cannot be found.";
        break;
      case EE.ERR_FILE_PERMISSION:
        msg = "Oops! You don't have permission to access";
        break;      
      default:
        msg = "Oops! An unknown error has occurred.";
        break;
    }

    return msg;
  }
  sendResponse(res) {
    res.status(this.httpStatusHeader())
      .json({
        status: this.httpStatusResponse(),
        message: (this.originalMessage) ? this.originalMessage : this.httpDefaultResponse()
      });
  }
}

EE.ERR_FLOW = "ERR_FLOW";
EE.ERR_UNKNOWN = "ERR_UNKNOWN";
EE.ERR_NOTFOUND = "ERR_NOTFOUND";
EE.ERR_FILE_PERMISSION = "ERR_FILE_PERMISSION";

EE.responseErrorUnknown = (res) => {  
  res.status(201).json({
    status: 503,
    message: "Oops! An unknown error has occurred."
  });
};

EE.responseErrorNotfound = (res) => {
  res.status(201).json({
    status: 404,
    message: "Sorry, this record cannot be found."
  });
};

EE.responseErrorFlow = (res, errMsg) => {
  res.status(201).json({
    status: 201,
    message: errMsg
  });  
}

EE.responseClientError = (res, errMsg) => {
  res.status(201).json({
    status: 400,
    message: errMsg
  });
}

module.exports = EE;
