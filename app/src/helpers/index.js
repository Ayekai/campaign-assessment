import config from '../services/config';
import moment from 'moment-timezone';

const appTimezone = 'UTC';

const getConfig = (key, root = '') => {
  let cfg = config;
  
  if (root != '') {
    cfg = config.get(root);    
  }  
  
  return cfg[key];  
};

const isExipred = (requestedTime) => {
  let current_time = getcurrent_time();
  let start_time = convertToAppDateTime(requestedTime);
  
  let minutes = (current_time.diff(start_time, 'minutes'));
  let expiresMinutes = getConfig('expiresMinutes', 'common');
  
  if (minutes > expiresMinutes) {
    return true;
  }
  return false;
};

const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

const convertToAppDateTime = (inputDateTime) => {
  return moment.tz(inputDateTime, appTimezone);  
};

const getcurrent_time = () => {  
  return moment().tz(appTimezone);
};

const isValidPeriod = (startDate, endDate) => {
  let current_time = getcurrent_time();
  const fromDate = convertToAppDateTime(startDate);
  const toDate = convertToAppDateTime(endDate);
  
  return current_time.isBetween(fromDate, toDate) ? true : false;
};

exports.asyncMiddleware = asyncMiddleware;
exports.getConfig = getConfig;
exports.isExipred = isExipred;
exports.isValidPeriod = isValidPeriod;
exports.getcurrent_time = getcurrent_time;