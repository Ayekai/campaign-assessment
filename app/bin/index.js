
/**
 * Module dependencies.
 */

const app = require('../src/app'),  
  http = require('http');

  const path = require('path');
import conf from '../src/services/config';

/**
* Normalize a port into a number, string, or false.
*/

const normalizePort = (val) => {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

/**
 * Get port from environment and store in Express.
 */
// let port = normalizePort(process.env.PORT || '9200');
let port = normalizePort( conf.get('port'));

app.set('port', port);

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');


/**
 * Create HTTP server.
 */
let server = http.createServer(app);

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof app.get('port') === 'string' ?
    'Pipe '.concat(app.get('port')) :
    'Port '.concat(app.get('port'));

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.log(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.log(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = () => {
  let addr = server.address(),
    bind = typeof addr === 'string' ?
      'pipe '.concat(addr) :
      'port '.concat(addr.port);

  console.log('Listening on '.concat(bind));
};

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => {
  console.log("server running at :".concat(port));
});

server.on('error', onError);
server.on('listening', onListening);

